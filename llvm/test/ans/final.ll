; ModuleID = 'file1.ll'
source_filename = "file1.c"
target datalayout = "e-m:e-p270:32:32-p271:32:32-p272:64:64-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

%struct._IO_FILE = type { i32, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, %struct._IO_marker*, %struct._IO_FILE*, i32, i32, i64, i16, i8, [1 x i8], i8*, i64, %struct._IO_codecvt*, %struct._IO_wide_data*, %struct._IO_FILE*, i8*, i64, i32, [20 x i8] }
%struct._IO_marker = type opaque
%struct._IO_codecvt = type opaque
%struct._IO_wide_data = type opaque

@.str = private unnamed_addr constant [3 x i8] c"%d\00", align 1
@.str.1 = private unnamed_addr constant [7 x i8] c"%d %d\0A\00", align 1
@stderr = external dso_local global %struct._IO_FILE*, align 8
@.str.2 = private unnamed_addr constant [30 x i8] c"Signatures are not matching \0A\00", align 1
@.str.3 = private unnamed_addr constant [14 x i8] c"equal %d %d \0A\00", align 1
@.str.4 = private unnamed_addr constant [24 x i8] c"s  %d d  %d g  %d  d %d\00", align 1
@Prasad = common global i32 0
@d = common global i32 0

; Function Attrs: noinline nounwind optnone uwtable
define dso_local i32 @main() #0 {
entry:
  store i32 3, i32* @d, align 4
  store i32 1, i32* @Prasad, align 4
  %retval = alloca i32, align 4
  %one = alloca i32, align 4
  %two = alloca i32, align 4
  %result = alloca i32, align 4
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %one, align 4
  store i32 2, i32* %two, align 4
  %0 = load i32, i32* %one, align 4
  %1 = load i32, i32* %two, align 4
  %call = call i32 @max(i32 %0, i32 %1)
  store i32 %call, i32* %result, align 4
  %2 = load i32, i32* %result, align 4
  %tobool = icmp ne i32 %2, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* @d, align 4
  %3 = load i32, i32* @Prasad, align 4
  %4 = xor i32 %3, 3
  store i32 %4, i32* @Prasad, align 4
  call void @print_bb(i32 2, i32 3, i32 %4, i32 99)
  %5 = call i32 @__ctt_error(i32 %4, i32 2)
  %6 = load i32, i32* %result, align 4
  %call1 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str, i64 0, i64 0), i32 %6)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %7 = load i32, i32* @Prasad, align 4
  %8 = xor i32 %7, 1
  %9 = load i32, i32* @d, align 4
  %10 = xor i32 %8, %9
  store i32 %10, i32* @Prasad, align 4
  call void @print_bb(i32 3, i32 1, i32 %10, i32 %9)
  %11 = call i32 @__ctt_error(i32 %10, i32 3)
  ret i32 0
}

; Function Attrs: noinline nounwind optnone uwtable
define dso_local i32 @max(i32 %x, i32 %y) #0 {
entry:
  %x.addr = alloca i32, align 4
  %y.addr = alloca i32, align 4
  %k = alloca i32, align 4
  store i32 %x, i32* %x.addr, align 4
  store i32 %y, i32* %y.addr, align 4
  %0 = load i32, i32* %x.addr, align 4
  %1 = load i32, i32* %y.addr, align 4
  %cmp = icmp sgt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %x.addr, align 4
  store i32 %2, i32* %k, align 4
  br label %if.end

if.else:                                          ; preds = %entry
  %3 = load i32, i32* %y.addr, align 4
  store i32 %3, i32* %k, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %4 = load i32, i32* %k, align 4
  ret i32 %4
}

declare dso_local i32 @printf(i8*, ...) #1

; Function Attrs: noinline nounwind optnone uwtable
define dso_local i32 @__ctt_error(i32 %a, i32 %b) #0 {
entry:
  %a.addr = alloca i32, align 4
  %b.addr = alloca i32, align 4
  store i32 %a, i32* %a.addr, align 4
  store i32 %b, i32* %b.addr, align 4
  %0 = load i32, i32* %a.addr, align 4
  %1 = load i32, i32* %b.addr, align 4
  %cmp = icmp ne i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %a.addr, align 4
  %3 = load i32, i32* %b.addr, align 4
  %call = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.1, i64 0, i64 0), i32 %2, i32 %3)
  %4 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 8
  %call1 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %4, i8* getelementptr inbounds ([30 x i8], [30 x i8]* @.str.2, i64 0, i64 0))
  br label %if.end

if.else:                                          ; preds = %entry
  %5 = load i32, i32* %a.addr, align 4
  %6 = load i32, i32* %b.addr, align 4
  %call2 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([14 x i8], [14 x i8]* @.str.3, i64 0, i64 0), i32 %5, i32 %6)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret i32 0
}

declare dso_local i32 @fprintf(%struct._IO_FILE*, i8*, ...) #1

; Function Attrs: noinline nounwind optnone uwtable
define dso_local void @print_bb(i32 %id, i32 %d, i32 %G, i32 %D) #0 {
entry:
  store i32 4, i32* @Prasad, align 4
  %id.addr = alloca i32, align 4
  %d.addr = alloca i32, align 4
  %G.addr = alloca i32, align 4
  %D.addr = alloca i32, align 4
  store i32 %id, i32* %id.addr, align 4
  store i32 %d, i32* %d.addr, align 4
  store i32 %G, i32* %G.addr, align 4
  store i32 %D, i32* %D.addr, align 4
  %0 = load i32, i32* %id.addr, align 4
  %1 = load i32, i32* %d.addr, align 4
  %2 = load i32, i32* %G.addr, align 4
  %3 = load i32, i32* %D.addr, align 4
  %call = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([24 x i8], [24 x i8]* @.str.4, i64 0, i64 0), i32 %0, i32 %1, i32 %2, i32 %3)
  ret void
}

attributes #0 = { noinline nounwind optnone uwtable "disable-tail-calls"="false" "frame-pointer"="all" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "disable-tail-calls"="false" "frame-pointer"="all" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 12.0.0 (https://github.com/llvm/llvm-project.git e2dd86bbfcb4c1888d5e0ff6256a51c906e621cb)"}
