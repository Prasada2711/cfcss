//===- Hello.cpp - Example code from "Writing an LLVM Pass" ---------------===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//
//
// This file implements two versions of the LLVM "Hello World" pass described
// in docs/WritingAnLLVMPass.html
//
//===----------------------------------------------------------------------===//
#include "llvm/IR/CFG.h"
#include "llvm/IR/GlobalVariable.h"
#include "llvm/ADT/Statistic.h"
#include "llvm/IR/Function.h"
#include "llvm/Pass.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/Instructions.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/IR/Type.h"
#include "llvm/IR/DerivedTypes.h"
#include "llvm/IR/LLVMContext.h"
#include<map>
#include "llvm/IR/InstrTypes.h"
#include<iostream>
#include "llvm/IR/Value.h"
using namespace llvm;

#define DEBUG_TYPE "hello"

STATISTIC(HelloCounter, "Counts number of functions greeted");

namespace {
  // Hello - The first implementation, without getAnalysisUsage.
  struct Hello : public FunctionPass {
    static char ID; // Pass identification, replacement for typeid
    Hello() : FunctionPass(ID) {}

    bool runOnFunction(Function &F) override {
	 if(F.getName()=="__ctt_error")
		 return false;
	 if(F.getName()=="print__bb")
		 return false;
	 if(F.getName()=="max")
		 return false;
    	auto ModuleOb=F.getParent();  
	auto &firstBB=F.getEntryBlock();
    	llvm::IRBuilder<> builder(ModuleOb->getContext());
	BasicBlock::iterator it=firstBB.begin();
	builder.SetInsertPoint(&firstBB,it); 
	
	std::string Name="Prasad";
	ModuleOb->getOrInsertGlobal(Name, builder.getInt32Ty());
	GlobalVariable *gVar = ModuleOb->getNamedGlobal(Name);
	gVar->setLinkage(GlobalValue::CommonLinkage);
	gVar->setInitializer(builder.getInt32(0));
	auto Zero = ConstantInt::get(ModuleOb->getContext(), APInt(32, 0));
//	builder.CreateStore(Zero, gVar);
std::string name1="d";
	ModuleOb->getOrInsertGlobal(name1, builder.getInt32Ty());
        GlobalVariable *gVar1 = ModuleOb->getNamedGlobal(name1);
	gVar1->setLinkage(GlobalValue::CommonLinkage);                      
	gVar1->setInitializer(builder.getInt32(0));
      // builder.CreateStore(Zero,gVar1);	
	
	static int a=1;
	std::map<BasicBlock *,int> map1;
	 for(BasicBlock &BB:F)
	{
		map1[&BB]=a;
		a++;
	}
	for(BasicBlock &BB:F)
	{	
		Value * unique1=ConstantInt::get(ModuleOb->getContext(),APInt(32,map1[&BB]));
		if(BB.getName()=="entry"){
			builder.CreateStore(unique1,gVar);
			continue;}
		else
		{
		
		
			Value * unique2;
			Value *DD;
			if(BB.getSinglePredecessor())
				unique2=ConstantInt::get(ModuleOb->getContext(),APInt(32,map1[BB.getSinglePredecessor()]));
			else
			{
				BasicBlock * first=*pred_begin(&BB);
				BasicBlock::iterator it2=first->begin();
				builder.SetInsertPoint(first,it2);
				builder.CreateStore(Zero,gVar1);
				unique2=ConstantInt::get(ModuleOb->getContext(),APInt(32,map1[first]));
				auto it3=pred_begin(&BB);
				it3++;

				for (auto it4 = it3, et = pred_end(&BB); it4 != et; ++it4)
				{
				 BasicBlock* predecessor = *it4;
		 		 BasicBlock::iterator it5=predecessor->begin();
				 builder.SetInsertPoint(predecessor,it5);
			//	Value * gb=ConstantInt::get(ModuleOb->getContext(),APInt(32,map1[first]));
		 		 DD=builder.CreateXor(unique2,map1[predecessor]);
		 		 builder.CreateStore(DD,gVar1);
  				}	
			 }	
	BasicBlock::iterator it1=BB.begin();
	builder.SetInsertPoint(&BB,it1);    			std::vector<Value *> args;
			std::vector<Value *> args2;
			auto dif=builder.CreateXor(unique1,unique2);
			Value * G_old=builder.CreateLoad(builder.getInt32Ty(),gVar);
			Value * G_new=builder.CreateXor(G_old,dif);
		//	Value * G_new1;	
			args2.push_back(unique1);
			args2.push_back(dif);
			if(pred_size(&BB)>1)
			{
				Value * D=builder.CreateLoad(builder.getInt32Ty(),gVar1);
				Value * G_new1=builder.CreateXor(G_new,D);
				args.push_back(G_new1);
				args.push_back(unique1);
				args2.push_back(G_new1);
				args2.push_back(D);
				builder.CreateStore(G_new1,gVar);
			}
			else
			{	
				Value * dummy=ConstantInt::get(ModuleOb->getContext(),APInt(32,99)); 
				args.push_back(G_new);
				args.push_back(unique1);
				args2.push_back(G_new);
				args2.push_back(dummy);
				builder.CreateStore(G_new,gVar);
			}
			
			Function * fun1=ModuleOb->getFunction("print_bb");
			builder.CreateCall(fun1,args2);
			Function * fun = ModuleOb->getFunction("__ctt_error");
			builder.CreateCall(fun,args);
		}
	
		//auto ai=builder.CreateAlloca(Type::getInt32Ty(ModuleOb->getContext()), NULL, "local");
		//builder.CreateStore(unique,ai);
	
    }
    	    
      return true;
    }
  };
}

char Hello::ID = 0;
static RegisterPass<Hello> X("hello", "Hello World Pass");

namespace {
  // Hello2 - The second implementation with getAnalysisUsage implemented.
  struct Hello2 : public FunctionPass {
    static char ID; // Pass identification, replacement for typeid
    Hello2() : FunctionPass(ID) {}

    bool runOnFunction(Function &F) override {
      ++HelloCounter;
      errs() << "Hello: ";
      errs().write_escaped(F.getName()) << '\n';
      return false;
    }

    // We don't modify the program, so we preserve all analyses.
    void getAnalysisUsage(AnalysisUsage &AU) const override {
      AU.setPreservesAll();
    }
  };
}

char Hello2::ID = 0;
static RegisterPass<Hello2>
Y("hello2", "Hello World Pass (with getAnalysisUsage implemented)");
